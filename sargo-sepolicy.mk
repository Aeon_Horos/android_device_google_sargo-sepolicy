BOARD_PLAT_PUBLIC_SEPOLICY_DIR := device/google/sargo-sepolicy/public
BOARD_PLAT_PRIVATE_SEPOLICY_DIR := device/google/sargo-sepolicy/private

# vendors
BOARD_SEPOLICY_DIRS += device/google/sargo-sepolicy/vendor/qcom/common
BOARD_SEPOLICY_DIRS += device/google/sargo-sepolicy/vendor/qcom/sdm710
BOARD_SEPOLICY_DIRS += device/google/sargo-sepolicy/vendor/google
BOARD_SEPOLICY_DIRS += device/google/sargo-sepolicy/vendor/verizon
